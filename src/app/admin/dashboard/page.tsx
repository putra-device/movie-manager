import Card from '@/components/Card'
import React from 'react'

type Props = {}

type WidgetProps = {
  title: string,
  quantity: number
}

const DashboardWidget = ({ title, quantity}: WidgetProps) => {
  return (
    <Card className='px-4 py-3 grid'>
      <h5 className='text-lg mb-3'>{ title }</h5>
      <span className='text-5xl justify-self-end'>{ quantity }</span>
    </Card>
  )
}

const Page = (props: Props) => {
  return (
    <div className="container flex min-h-screen flex-col p-4 items-center mx-auto max-w-5xl">
      <div className="flex flex-row justify-between w-full gap-x-3">
        <div className="w-6/12">
          <DashboardWidget title='Movies' quantity={50} />
        </div>
        <div className="w-6/12">
          <DashboardWidget title='Members' quantity={100} />
        </div>
      </div>

      <div className="w-full">
        <Card className='px-4 py-3'>
          <h5 className='txt-lg mb-3'>Top 10 Movies</h5>
          <table className='table-auto w-full'>
            <thead>
              <tr>
                <th className='w-[30px] text-center'>#</th>
                <th className='text-left'>Title</th>
                <th className='text-left'>Favorites</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Power ranger</td>
                <td></td>
              </tr>
              <tr>
                <td>2</td>
                <td>Naruto</td>
                <td></td>
              </tr>
              <tr>
                <td>3</td>
                <td>Bleach</td>
                <td></td>
              </tr>
              <tr>
                <td>4</td>
                <td>Ultramen Tiga</td>
                <td></td>
              </tr>
              <tr>
                <td>5</td>
                <td>Dragon ball</td>
                <td></td>
              </tr>
              <tr>
                <td>6</td>
                <td>One Piece</td>
                <td></td>
              </tr>
              <tr>
                <td>7</td>
                <td>Avengers</td>
                <td></td>
              </tr>
              <tr>
                <td>8</td>
                <td>Sinchan</td>
                <td></td>
              </tr>
              <tr>
                <td>9</td>
                <td>Cek Toko Sebelah</td>
                <td></td>
              </tr>
              <tr>
                <td>10</td>
                <td>Keluarga Cemara</td>
                <td></td>
              </tr>
            </tbody>
          </table>

        </Card>
      </div>
        
    </div>
  )
}

export default Page