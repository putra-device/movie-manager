import Card from '@/components/Card'
import Link from 'next/link'
import React from 'react'

type Props = {}

const Movies = (props: Props) => {
  return (
    <div className="container flex min-h-screen flex-col p-4 items-center mx-auto max-w-5xl">
      <div className="w-full my-3 py-3 px-4 flex flex-col">
        <h2 className='text-2xl'>Movies</h2>
        <ul className='breadcrumb'>
          <li>
            <Link href="/admin/dashboard">Dashboard</Link>
          </li>
          <li>
            Movie
          </li>
        </ul>
      </div>
      <div className="w-full my-3 py-3 px-4">
        <Card className='px-4 py-3'>
          <div className="mb-5 flex flex-row justify-between">
            <div className="w-[50%] flex flex-row  rounded border border-solid border-[#1e293b] ">
              <input type="text" className="w-full py-2 px-4" placeholder='Search...' />
              <button className='px-2'><i className="fa fa-search"></i></button>
            </div>
            <Link href="/admin/movies/create" className='py-2 px-4 bg-gradient-to-b rounded-lg bg-primary text-[#fff]'><i className="fa fa-plus"></i> Add Movie</Link>
          </div>
          <table className='table-auto w-full border-spacing-0'>
            <thead>
              <tr>
                <th className='w-[30px] text-center py-2 px-2 border'>#</th>
                <th className='text-left py-2 px-2 border'>Title</th>
                <th className='text-left py-2 px-2 border'>Genres</th>
                <th className='text-left py-2 px-2 border'>Release Date</th>
                <th className='text-left py-2 px-2 border'>Runtime</th>
                <th className='text-left py-2 px-2 border'></th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className='w-[30px] text-left py-2 px-2 border'>1</td>
                <td className='text-left py-2 px-2 border'>Power ranger</td>
                <td className='text-left py-2 px-2 border'></td>
                <td className='text-left py-2 px-2 border'></td>
                <td className='text-left py-2 px-2 border'></td>
                <td className='text-left py-2 px-2 border'></td>
              </tr>
            </tbody>
          </table>

        </Card>
      </div>
    </div>
  )
}

export default Movies