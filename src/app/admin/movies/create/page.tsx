import React from 'react'
import MovieForm from '../components/MovieForm'
import Card from '@/components/Card'
import Link from 'next/link'

type Props = {}

const NewMovie = (props: Props) => {
  return (
    <div className="container flex min-h-screen flex-col p-4 items-center mx-auto max-w-5xl">
      <div className="w-full my-3 py-3 px-4 flex flex-col">
        <h2 className='text-2xl'>Movies</h2>
        <ul className='breadcrumb'>
          <li>
            <Link href="/admin/dashboard">Dashboard</Link>
          </li>
          <li>
            <Link href="/admin/movies">Movies</Link>
          </li>
          <li>Create New</li>
        </ul>
      </div>
      <div className="w-full my-3 py-3 px-4">
        <Card className='px-4 py-3'>
          <MovieForm />
        </Card>
      </div>
    </div>
          
  )
}

export default NewMovie