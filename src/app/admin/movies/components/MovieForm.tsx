"use client"

import Image from 'next/image'
import React, { ChangeEvent, ChangeEventHandler, FormEvent, FormEventHandler, useMemo, useState } from 'react'

type Props = {}

interface HtmlFileInputElement extends HTMLInputElement {
  files: FileList;
}

const MovieForm = (props: Props) => {
  const [image, setImage] = useState<string|null>(null)

  const ImagePreview = () => {
    if(image == null) return <></>

    return <img src={image} alt='image preview' />
  }

  const changeImageHandler = (event: ChangeEvent<HtmlFileInputElement>) => {
    if(event.target.files.length > 0){
      setImage(
        URL.createObjectURL(event.target.files[0])
      );
    } 
  }

  const submitHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    // console.log(event.target);
  }

  return (
    <form onSubmit={submitHandler}>
      <div className="flex flex-row gap-x-2 mb-3">
        <label htmlFor="title" className="text-base py-2 w-2/12">Title</label>
        <input type="text" id='title' name='title' className="py-2 px-4 border rounded grow" required />
      </div>
      <div className="flex flex-row gap-x-2 mb-3">
        <label htmlFor="description" className="text-base py-2 w-2/12">Description</label>
        <textarea id='description' name='description' className="py-2 px-4 border rounded grow" rows={3} required />
      </div>
      <div className="flex flex-row gap-x-2 mb-3">
        <label htmlFor="release_date" className="text-base py-2 w-2/12">Release Date</label>
        <input id='release_date' name='release_date' type="date" className="py-2 px-4 border rounded grow" required />
      </div>
      <div className="flex flex-row gap-x-2 mb-3">
        <label htmlFor="runtime" className="text-base py-2 w-2/12">Runtime</label>
        <input id='runtime' name='runtime' type="number" className="py-2 px-4 border rounded grow" required />
      </div>
      <div className="flex flex-row gap-x-2 mb-3">
        <label htmlFor="revenue" className="text-base py-2 w-2/12">Revenue</label>
        <input id="revenue" name='revenue' type="number" className="py-2 px-4 border rounded grow" required />
      </div>
      <div className="flex flex-row gap-x-2 mb-3">
        <label htmlFor="poster" className="text-base py-2 w-2/12">Poster</label>
        <input id="poster" name='poster' type="file" className="py-2 px-4 border rounded grow" onChange={changeImageHandler} required />
      </div>
      <div className="flex flex-row gap-x-2 mb-3 justify-end">
        <div className="w-10/12">
          <ImagePreview />
        </div>
      </div>
      <div className="flex flex-row gap-x-2 mb-3 justify-end">
        <button className="py-2 px-4 rounded bg-[#ccc] text-[#fff]" type='reset'>Cancel</button>
        <button className="py-2 px-4 rounded bg-success text-[#fff]" type='submit'>Sumbit</button>
      </div>
    </form>
  )
}

export default MovieForm