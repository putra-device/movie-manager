import { PropsWithChildren } from "react";

interface CardProps extends PropsWithChildren {
    className?: string
}
const Card = ({ className = '', children } : CardProps) => {
  const cardClass = className + ' rounded overflow-hidden shadow-lg mx-auto my-8';
  return <>
    <div className={cardClass}>
      { children }
    </div>
  </>
}

export default Card;