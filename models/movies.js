'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class movies extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  movies.init({
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    releaseDate: DataTypes.DATE,
    runtime: DataTypes.NUMBER,
    revenue: DataTypes.NUMBER,
    votes: DataTypes.NUMBER,
    posterUrl: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'movies',
  });
  return movies;
};